module.exports = {
  'env': {
    'commonjs': true,
    'es6': true,
    'browser': true,
  },
  'extends': [
    'eslint:recommended',
    'plugin:react/recommended',
  ],
  'parserOptions': {
    'ecmaFeatures': {
      'experimentalObjectRestSpread': true,
      'jsx': true,
    },
    'sourceType': 'module'
  },
  'plugins': [
    'react',
  ],
  'rules': {
    'no-console': [
      'warn',
      {
        'allow': ['warn', 'error'],
      },
    ],
    'indent': [
      'warn',
      2,
      { 'SwitchCase': 1 },
    ],
    'linebreak-style': [
      'error',
      'unix',
    ],
    'quotes': [
      'error',
      'single',
    ],
    'semi': [
      'error',
      'never',
    ],
    'no-irregular-whitespace': [
      'error',
      {
        'skipComments': true,
        'skipStrings': true,
      },
    ],
  },
  'globals': {
    'process': false,
  }
}
